# This file is a template, and might need editing before it works on your project.
FROM python:2.7

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

CMD ["python", "app.py"]

#!/usr/bin/env python
# -*- coding:utf-8 -*-

from os import listdir
from os.path import isfile, join
import pandas as pd
import pyneb as pn
from pyneb.utils.physics import IP
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from pyneb.utils.misc import get_reduced, get_reduced_dic
pn.config.use_multiprocs()




class abund_quim(object):
    #Esta clase calculara los parámetros físicos de la nebulosa, las abundancias ionicas y las abundancias totales para cada elemento.

    #En esta funcion se cambian los archivos de entrada
    def __init__(self, obs, file_Format = 'lines_in_rows', extincion_law = 'CCM89', correct = True, i_obs=None, delimiter = ' ', NMonteCarlo=500):
        """
        Esta función instancia la clase de abundancias químicas.

        Nota:
            Regresa una variable global con el nombre de las observaciones
        Args:
            obs           : Nombre del archivo con las observaciones
            extincion_law : Ley de extinción que se usará. Por defecto; CCM89
            correct       : Estan corregidas por extinción las intensidades; por defecto True
        """
        obs_tmp = pn.Observation(obs, fileFormat = file_Format, corrected = correct, errIsRelative = False, delimiter = delimiter)
        if i_obs is None:
            self.obs = obs_tmp
        else:
            self.obs = pn.Observation()
            lines = [l for l in obs_tmp.lines]
            for l in lines:
                l.obsIntens = np.array([l.obsIntens[i_obs]])
            self.obs.lines = lines
            self.obs.names.append(obs_tmp.names[i_obs])
        if NMonteCarlo > 1:
            self.obs.addMonteCarloObs(NMonteCarlo)
        print "Observaciones leídas"
        if not correct:
            self.obs.extinction.law = extincion_law
            self.obs.def_EBV(label1='H1_6563A', label2='H1_4861A', r_theo=2.85)
            self.obs.correctData()
            print "Observaciones desenrrojecidas"

    def DATOS_ATOMICOS(self):
        DataFileDict = {'H1': {'rec': 'h_i_rec_SH95.hdf5'},
		'He1': {'rec':'he_i_rec_Pal12-Pal13.hdf5'},
		'He2': {'rec':'he_ii_rec_SH95.hdf5'},
		'O2': {'atom': 'o_ii_atom_WFD96.dat', 'coll': 'o_ii_coll_P06-T07.dat'},
		'O3': {'atom': 'o_iii_atom_GMZ97-WFD96.dat', 'coll': 'o_iii_coll_AK99.dat'},
		'N2': {'atom': 'n_ii_atom_GMZ97-WFD96.dat', 'coll': 'n_ii_coll_LB94.dat'},
		'S2': {'atom': 's_ii_atom_KKFBL14.dat', 'coll': 's_ii_coll_RBS96.dat'},
		'S3': {'atom': 's_iii_atom_MZ82b-HSC95-LL93.dat', 'coll': 's_iii_coll_GMZ95.dat'},
		'Ar3': {'atom': 'ar_iii_atom_M83-KS86.dat', 'coll': 'ar_iii_coll_MB09.dat'},
		'Ar4': {'atom': 'ar_iv_atom_CK73.dat', 'coll': 'ar_iv_coll_M83.dat'},
		'Cl2': {'atom': 'cl_ii_atom_MZ83.dat', 'coll': 'cl_ii_coll_WB02.dat'},
		'Cl3': {'atom': 'cl_iii_atom_Fal99.dat', 'coll': 'cl_iii_coll_RBK01.dat'},
		'Cl4': {'atom': 'cl_iv_atom_KS86-MZ82-EM84.dat', 'coll': 'cl_iv_coll_GMZ95.dat'}
        }
        pn.atomicData.setDataFileDict(DataFileDict)
        pn.config.pypic_path = './pypics/'
        atomos = pn.getAtomDict(atom_list=self.obs.getUniqueAtoms())
        print "Datos atómicos cambiados"
        return atomos


    def init_diags(self):
        #Creamos la variable diags que contendra a los diagnosticos disponibles en PyNeb
        diags = pn.Diagnostics()
        #Llamamos a lso diagnosticos que usaremos durante el codigo. Aqui se crean los diagnosticos si no estan
        diags.addDiag(['[OII] 3726/3729',
                        '[OII] 3727+/7325+',
						'[OIII] 4363/5007+',
						'[SII] 6731/6716',
						'[SII] 4072+/6720+',
						'[NII] 5755/6584+',
						'[ArIV] 4740/4711',
						'[ClIII] 5538/5518',
                        '[OII] 3727+/7325+b'])
        #diags.addDiag('[OII] 3727+/7325+b',('O2', '(L(3726) + L(3729))/(L(7319)+L(7320)+L(7330)+L(7331))', 'RMS([E(3726), E(3729), E(7319), E(7320), E(7330), E(7331)])'))
        return diags

    def getTemDen(self, i_obs=None, print_ = False):
        pn.log_.message('Entering', calling = 'getTemDen')
        #Llamamos a la funcion init_diags que contiene los cocientes de lineas que usaremos
        diags = self.init_diags()
        #Creamos un diccionario donde se meteran los valores de densidad y temperatura
        temp = {}
        dens = {}

        #Usamos crosstemden para obtener una temperatura y densidad que se guardan en el diccionario para los cocientes necesarios
        theo_temp = raw_input('Do you want to use a theoretical relationship? (Peimbert, A et al 2002) [yes/no] ')
        if theo_temp == 'yes':
            temp['O3_S2'], dens['S2'] = diags.getCrossTemDen('[OIII] 4363/5007+', '[SII] 6731/6716', obs = self.obs)
            temp_O3 = temp['O3_S2']
            temp['O2'] = 2430. + temp_O3*(1.031 - temp_O3/54350.)
            temp_O2 = temp['O2']
            dens['S2_th'] = diags.atomDict['S2'].getTemDen(self.obs.getIntens()['S2_6731A'] / self.obs.getIntens()['S2_6716A'], tem=temp['O2'], to_eval = 'L(6731)/L(6716)')
            den = dens['S2_th']
            temp_N2 = np.nan
        else:
            try:
                temp['tempO3'], dens['densAr4'] = diags.getCrossTemDen('[OIII] 4363/5007+', '[ArIV] 4740/4711', obs = self.obs)
            except TypeError:
                print('Observations could not have [OIII] or [ArIV] lines')
                pass
            try:
                temp['tempO3_Cl3'], dens['densCl3'] = diags.getCrossTemDen('[OIII] 4363/5007+', '[ClIII] 5538/5518', obs = self.obs)
            except TypeError:
                print('Observations could not have [OIII] or [ClIII] lines')
                pass
            try:
                temp['tempO3_S2'], dens['densS2'] = diags.getCrossTemDen('[OIII] 4363/5007+', '[SII] 6731/6716', obs = self.obs)
            except TypeError:
                print('Observations could not have [OIII] or [SII] lines')
                pass
            try:
                temp['tempN2'], dens['densS2_N2'] = diags.getCrossTemDen('[NII] 5755/6584+', '[SII] 6731/6716', obs = self.obs)
            except TypeError:
                print('Observations could not have [NII] or [SII] lines')
                pass
            try:
                temp['tempN2_O2'], dens['densO2'] = diags.getCrossTemDen('[NII] 5755/6584+', '[OII] 3726/3729', obs = self.obs)
            except TypeError:
                print('Observations could not have [NII] or [OII] lines')
                pass
            try:
                temp['tempO2'], dens['densS2_O2'] = diags.getCrossTemDen('[OII] 3727+/7325+b', '[SII] 6731/6716', obs = self.obs)
            except TypeError:
                print('Observations could not have [OII] or [SII] lines')

            O3_keys = [temp.keys()[i].startswith('tempO3') for i in range(len(temp.keys()))]
            temp_O3 = np.mean(np.array(temp.values())[O3_keys])

            N2_keys = [temp.keys()[i].startswith('tempN2') for i in range(len(temp.keys()))]
            temp_N2 = np.mean(np.array(temp.values())[N2_keys])

            try:
                temp_O2 = temp['tempO2']
            except KeyError:
                temp_O2 = np.nan
                print('No Te[OII]')

            dens_keys = [temp.keys()[i].startswith('densS2') for i in range(len(temp.keys()))]
            den = np.mean(np.array(temp.values())[dens_keys])

        print "Temperaturas y densidad obtenidas"
        return temp_O3, temp_N2, temp_O2, temp, den, dens

    def getAbund(self, Hb = 100., print_=False):
        #Llamamos a la funicion de datos atomicos
        atomos = self.DATOS_ATOMICOS()
        #Llamamos a las funcion de temperatura y densidad
        temp_O3, temp_N2, temp_O2, temp, den, dens = self.getTemDen()
        #Creamos un diccionario vacio donde guardaremos las abundancias ionicas CELs y RLS
        ion_ab_dic = {}
        #Escribimos las lineas que usaremos para obtener las abundancias ionicas
        ab_labels = ['N2_6548A', 'N2_6584A', 'N2_5755A',
					'O2_3726A', 'O2_3729A', 'O2_3727A+', 'O2_7319A', 'O2_7320A', 'O2_7330A', 'O2_7331A', 'O3_4959A', 'O3_5007A', 'O3_4363A',
                    'Ne3_3869A', 'S2_6716A', 'S2_6731A', 'S2_4069A', 'S2_4076A', 'S3_6312A',
                    'Ar3_7136A', 'Ar4_4711A', 'Ar4_4740A',
					'Cl2_8579A', 'Cl2_9124A', 'Cl3_5518A','Cl3_5538A', 'Cl4_8046A', 'Cl4_7531A']
        print "Longitudes de onda para abundancias ionicas detectadas"
        for line in self.obs.getSortedLines():
            if line.label in ab_labels and line.is_valid:
                # Se usara T[N II] si el ion tiene IP < IP_cut y T[O III] si es mayor
                IP_cut = 15.
                if IP[line.atom] < IP_cut:
                    temp = temp_N2
                    dens = den
                else:
                    temp = temp_O3
                    dens = den
                if line.atom in atomos:
                    ab = atomos[line.atom].getIonAbundance(line.obsIntens, temp, dens, to_eval=line.to_eval, Hbeta=Hb)
                if print_:
                    print '{0:13s} '.format(line.label) + ' '.join(['{0:>8.3f}'.format(t) for t in np.log10(ab)+12])
                if line.atom not in ion_ab_dic:
                    ion_ab_dic[line.atom] = []
                ab[ab == 0] = np.nan
                ion_ab_dic[line.atom].append(ab)
            else:
                pn.log_.warn('Missing atom to compute ionic abunce from {0}'.format(line.label), calling='getAbund')
        for atom in ion_ab_dic:
            ion_ab_dic[atom] = np.nanmean(ion_ab_dic[atom], axis = 0)

        he1 = pn.RecAtom('He', 1)
        he2 = pn.RecAtom('He', 2)
        line1 = self.obs.getLine('He', 1, wave = 5876)
        line2 = self.obs.getLine('He', 2, wave = 4686)
        ion_ab_dic['He1'] = he1.getIonAbundance(line1.obsIntens, temp_O3, dens, wave = 5876, Hbeta=Hb)
        ion_ab_dic['He2'] = he2.getIonAbundance(line2.obsIntens, temp_O3, dens, 4, 3, Hbeta=Hb)
        print "Abundancias ionicas calculadas"
        return ion_ab_dic

    def getElemAb(self, Hb = 100.):
        #Llamo a la funcion de las abundancias ionicas
        ion_ab_dic = self.getAbund(Hb = Hb)
        #ion_ab_dic = {'O2': np.array([np.nan, 3.70e-5]), 'O3': np.array([np.nan,1.17e-4]), 'N2': np.array([np.nan,9.87e-7]), 'Ne3': np.array([np.nan,2.30e-5]), 'S2': np.array([np.nan, 4.72e-7]), 'S3': np.array([np.nan, 1.81e-6]), 'Ar3': np.array([np.nan, 4.61e-7]), 'Ar4' :np.array([np.nan, 1.76e-7]), 'Cl3': np.array([np.nan, 3.12e-8]), 'He1': np.array([np.nan, 0.08128305161640995]), 'He2': np.array([np.nan, 0.00019952623149688828])}
        # Se crea un diccionario para guardar las abundancias totales
        elem_ab_dic = {}
        # Se crea un diccionario para guardar los erroes de las Abundancias totales
        err_elem_ab_dic = {}
        #Se crea un diccionario para los ICFs
        icf_dic = {}
        #Se crea un diccionario para el grado de ionizacion
        gi_dic = {}

        #Grados de ionizacion
        gio = ion_ab_dic['O3']/(ion_ab_dic['O2']+ion_ab_dic['O3'])
        tt1 = np.isnan(ion_ab_dic['He1'])
        tt2 = np.isnan(ion_ab_dic['He2'])
        gihe = ion_ab_dic['He2']/(ion_ab_dic['He1']+ion_ab_dic['He2'])
        gihe[tt1 & tt2] = np.nan
        gihe[tt2 & ~tt1] = 0.
        gihe[~tt2 & tt1] = np.nan
        gio2 = ion_ab_dic['O2']/(ion_ab_dic['O3'] + ion_ab_dic['O2'])
        gio3 = (ion_ab_dic['O2']+ion_ab_dic['O3']) / ion_ab_dic['O2']
        sgio = ion_ab_dic['S3']/(ion_ab_dic['S2']+ion_ab_dic['S3'])
        logO1O2= np.log10(ion_ab_dic['O2']/ion_ab_dic['O3'])
        gi_dic['O'] = gio
        gi_dic['He'] = gihe
        gi_dic['O_other'] = gio2
        gi_dic['O_inver'] = gio3
        gi_dic['Sgio'] = sgio
        gi_dic['logO1O2'] = logO1O2

        #Se calculan las abundancias
        ## Oxigeno ##
        sum_o = ion_ab_dic['O2'] + ion_ab_dic['O3']
        icfo = 10**((0.08*gihe + 0.006*(gihe**2))/(0.34 - 0.27*gihe)) # ICF Gloria
        icfo[(np.isnan(ion_ab_dic['He2']))] = 1.0  #Si no hay He II, ICF(O) = 1.0
        OH = sum_o*icfo
        #Se guardan las abundancias totales de oxigeno con y sin ICF
        elem_ab_dic['OH'] = np.log10(OH)+12.

        print "Abundancias de oxigeno obtenida"

        ## Nitrogeno ##
        sum_n = ion_ab_dic['N2']
        icf1n1 =  np.ones_like(sum_n) # ICF Peimbert&Costero(1969)
        icf2n2 =  10**(((8.478*gio)/(4.301+gio) + ((-1.528)*gio)) - 0.0357) # ICF Medina-Amayo et al. (2019)

        NH_1 = sum_n
        NO_2 = (sum_n/ion_ab_dic['O2'])*icf1n1 # ICF Peimbert&Costero(1969)
        NH_2 = NO_2*OH # ICF Peimbert&Costero(1969)
        NO_3 = (sum_n/ion_ab_dic['O2'])*icf2n2 # ICF Medina-Amayo et al. (2019)
        NH_3 = NO_3*OH # ICF Medina-Amayo et al. (2019)

        elem_ab_dic['NH_sum'] = 12. + np.log10(NH_1)
        elem_ab_dic['NH_pc69'] = 12. + np.log10(NH_2) # ICF Peimbert&Costero(1969)
        elem_ab_dic['NO_pc69'] = np.log10(NO_2) # ICF Peimbert&Costero(1969)
        elem_ab_dic['NO_ma19'] = np.log10(NO_3) # ICF Medina-Amayo et al. (2019)
        elem_ab_dic['NH_ma19'] = 12. + np.log10(NH_3) # ICF Medina-Amayo et al. (2019)

        err_elem_ab_dic['NO_pc69_inf'] = 0.11*np.ones_like(sum_o)
        err_elem_ab_dic['NO_pc69_sup'] = 0.05*np.ones_like(sum_o)
        err_elem_ab_dic['NO_ma19_inf'] = 0.1*np.ones_like(sum_o)
        err_elem_ab_dic['NO_ma19_sup'] = 0.1*np.ones_like(sum_o)
        # Errores Peimbert&Costero(1969)
        esxNO_KB = 10**(elem_ab_dic['NO_pc69'] + err_elem_ab_dic['NO_pc69_sup']) - 10**elem_ab_dic['NO_pc69']
        eixNO_KB = 10**elem_ab_dic['NO_pc69'] - 10**(elem_ab_dic['NO_pc69'] - err_elem_ab_dic['NO_pc69_inf'])
        esxNH_KB = ((NO_2+esxNO_KB)*OH) - (NO_2*OH)
        eixNH_KB = (NO_2*OH) - ((NO_2-eixNO_KB)*(OH))
        err_elem_ab_dic['NH_pc69_sup'] = np.log10(NH_2+esxNH_KB) - np.log10(NH_2)
        err_elem_ab_dic['NH_pc69_inf'] = np.log10(NH_2) - np.log10(NH_2-eixNH_KB)
        # Errores Medina-Amayo et al. (2019)
        esxNOh = 10**(elem_ab_dic['NO_ma19'] + err_elem_ab_dic['NO_ma19_sup']) - 10**elem_ab_dic['NO_ma19'] # esx= (N/O)max - N/Oreal donde N/Omax=N/O+es
        eixNOh = 10**elem_ab_dic['NO_ma19'] - 10**(elem_ab_dic['NO_ma19'] - err_elem_ab_dic['NO_ma19_inf'])
        esxNHh = ((NO_3+esxNOh)*OH) - (NO_3*OH)
        eixNHh = (NO_3*OH) - ((NO_3-eixNOh)*(OH))
        err_elem_ab_dic['NH_ma19_sup'] = np.log10(NH_3+esxNHh) - np.log10(NH_3)
        err_elem_ab_dic['NH_ma19_inf'] = np.log10(NH_3) - np.log10(NH_3-eixNHh)

        # ICFs N #
        icf_dic['NH_pc69'] = 10**(elem_ab_dic['NH_pc69']-12.)/10**(elem_ab_dic['NH_sum']-12.) # icf[NO_KB94] = 1.0
        icf_dic['NH_ma19'] = 10**(elem_ab_dic['NH_ma19']-12.)/10**(elem_ab_dic['NH_sum']-12.)
        icf_dic['NO_ma19'] = 10**(elem_ab_dic['NO_ma19'])/(ion_ab_dic['N2']/ion_ab_dic['O2'])

        print "Abundancias de nitrogeno obtenida"

        ## Neon ##
        sum_ne = ion_ab_dic['Ne3']
        icf1ne3 =  np.ones_like(sum_ne) # Peimbert & Costero 1969
        icf2ne3 =  1.2 - (0.2/((ion_ab_dic['O3'])/OH))  # Stasinska 1978
        for i, OH12 in zip(gio, elem_ab_dic['OH']):
            if (OH12 < 7.2):
                icf3ne3 =  (-0.385*(i))+(1.365)+(0.022/(i)) # Izotov et al 2006 lowZ, lowZ= (logOH12 < 7.2)
            elif (OH12 > 7.2) & (OH12 < 8.2):
                icf3ne3 =  (-0.405*(i))+(1.382)+(0.021/(i))  # Izotov et al 2006 intZ, intZ= (logOH12 > 7.2) & (logOH12 < 8.2)
            elif (OH12 >= 8.2):
                icf3ne3 =  (-0.591*(i))+(0.927)+(0.546/(i))  # Izotov et al 2006 highZ, highZ= (logOH12 >= 8.2)
        icf4ne3 = 10**(((-0.7)*((-0.4*logO1O2+1.4)**2.0))-(np.log10((-0.4*logO1O2)+1.4))+1.67) # Medina-Amayo et al. (2019)

        NeH_1 = sum_ne
        NeO_2 = sum_ne*icf1ne3/(ion_ab_dic['O3']) # Peimbert & Costero 1969
        NeH_2 = NeO_2*OH   # Peimbert & Costero 1969
        NeO_3 = sum_ne*icf2ne3/(ion_ab_dic['O3']) # Stasinska 1978
        NeH_3 = NeO_3*OH  # Stasinska 1978
        NeH_4 = sum_ne*icf3ne3 # Izotov et al 2006
        NeO_4 = NeH_4/OH # Izotov et al 2006
        NeO_6 = sum_ne*icf5ne3/(ion_ab_dic['O2']) # Medina-Amayo et al. (2019)
        NeH_6 = NeO_6*OH # Medina-Amayo et al. (2019)

        elem_ab_dic['NeH_sum'] = 12. + np.log10(NeH_1)
        elem_ab_dic['NeH_pc69'] = 12. + np.log10(NeH_2)
        elem_ab_dic['NeO_pc69'] = np.log10(NeO_2)
        elem_ab_dic['NeH_s78'] = 12. + np.log10(NeH_3)
        elem_ab_dic['NeO_s78'] = np.log10(NeO_3)
        elem_ab_dic['NeH_iz06'] = 12. + np.log10(NeH_4)
        elem_ab_dic['NeO_iz06'] = np.log10(NeO_4)
        elem_ab_dic['NeO_ma19'] = np.log10(NeO_6)
        elem_ab_dic['NeH_ma19'] = 12. + np.log10(NeH_6)

        err_elem_ab_dic['NeO_pc69_inf'] = 0.05*np.ones_like(sum_o) # Peimbert & Costero 1969
        err_elem_ab_dic['NeO_pc69_sup'] = 0.56*np.ones_like(sum_o) # Peimbert & Costero 1969
        err_elem_ab_dic['NeO_s78_inf'] = 0.22*np.ones_like(sum_o) # Stasinska 1978
        err_elem_ab_dic['NeO_s78_sup'] = 0.05*np.ones_like(sum_o) # Stasinska 1978
        if (OH12 < 7.2):
            err_elem_ab_dic['NeH_iz06_inf'] = np.nan*np.ones_like(sum_o)
            err_elem_ab_dic['NeH_iz06_sup'] = np.nan*np.ones_like(sum_o)
        elif (OH12 > 7.2) & (OH12 < 8.2):
            err_elem_ab_dic['NeH_iz06_inf'] = 0.0*np.ones_like(sum_o)
            err_elem_ab_dic['NeH_iz06_sup'] = 1.6*np.ones_like(sum_o)
        elif (OH12 >= 8.2):
            err_elem_ab_dic['NeH_iz06_inf'] = 0.6*np.ones_like(sum_o)
            err_elem_ab_dic['NeH_iz06_sup'] = 0.9*np.ones_like(sum_o)
        err_elem_ab_dic['NeO_ma19_inf'] = 0.23*np.ones_like(sum_o) # Medina-Amayo et al. (2019)
        err_elem_ab_dic['NeO_ma19_sup'] = 0.22*np.ones_like(sum_o) # Medina-Amayo et al. (2019)

        # Errores Peimbert&Costero(1969)
        esxNeO_PC = (10**elem_ab_dic['NeO_69'] + err_elem_ab_dic['NeO_pc69_sup']) - 10**elem_ab_dic['NeO_pc69']
        eixNeO_PC = 10**elem_ab_dic['NeO_pc69'] - 10**(elem_ab_dic['NeO_pc69'] - err_elem_ab_dic['NeO_pc69_inf'])
        esxNeH_PC = ((NeO_2+esxNeO_PC)*OH) - (NeO_2*OH)
        eixNeH_PC = (NeO_2*OH) - ((NeO_2-eixNeO_PC)*(OH))
        err_elem_ab_dic['NeH_pc69_sup'] = np.log10(NeH_2+esxNeH_PC) - np.log10(NeH_2)
        err_elem_ab_dic['NeH_pc69_inf'] = np.log10(NeH_2) - np.log10(NeH_2-eixNeH_PC)
        # Errores Stasinska 1978
        esxNeO_S = (10**elem_ab_dic['NeO_s78'] + err_elem_ab_dic['NeO_s78_sup']) - 10**elem_ab_dic['NeO_s78']
        eixNeO_S = 10**elem_ab_dic['NeO_s78'] - 10**(elem_ab_dic['NeO_s78'] - err_elem_ab_dic['NeO_s78_inf'])
        esxNeH_S = ((NeO_3+esxNeO_S)*OH) - (NeO_3*OH)
        eixNeH_S = (NeO_3*OH) - ((NeO_3-eixNeO_S)*(OH))
        err_elem_ab_dic['NeH_s78_sup'] = np.log10(NeH_3+esxNeH_S) - np.log10(NeH_3)
        err_elem_ab_dic['NeH_s78_inf'] = np.log10(NeH_3) - np.log10(NeH_3-eixNeH_S)
        # Errores Izotov et al 2006
        esxNeH_IZI = (10**elem_ab_dic['NeH_iz06'] + err_elem_ab_dic['NeH_iz06_sup']) - 10**elem_ab_dic['NeH_iz06']
        eixNeH_IZI = 10**elem_ab_dic['NeH_iz06'] - 10**(elem_ab_dic['NeH_iz06'] - err_elem_ab_dic['NeH_iz06_inf'])
        esxNeO_IZI = ((NeH_4+esxNeH_IZI)/OH) - (NeH_4/OH)
        eixNeO_IZI = (NeH_4/OH) - ((NeH_4-eixNeH_IZI)/(OH))
        err_elem_ab_dic['NeO_iz06_sup'] = np.log10(NeO_4+esxNeO_IZI) - np.log10(NeO_4)
        err_elem_ab_dic['NeO_iz06_inf'] = np.log10(NeO_4) - np.log10(NeO_4-eixNeO_IZI)
        #Errores Medina-Amayo et al. (2019)
        esxNeO = (10**elem_ab_dic['NeO_ma19'] + err_elem_ab_dic['NeO_ma19_sup']) - 10**elem_ab_dic['NeO_ma19']
        eixNeO = 10**elem_ab_dic['NeO_ma19'] - 10**(elem_ab_dic['NeO_ma19'] - err_elem_ab_dic['NeO_ma19_inf'])
        esxNeH = ((NeO_6+esxNeO)*OH) - (NeO_6*OH)
        eixNeH = (NeO_6*OH) - ((NeO_6-eixNeO)*(OH))
        err_elem_ab_dic['NeH_ma19_sup'] = np.log10(NeH_6+esxNeH) - np.log10(NeH_6)
        err_elem_ab_dic['NeH_ma19_inf'] = np.log10(NeH_6) - np.log10(NeH_6-eixNeH)

        # ICFs Ne #
        icf_dic['NeH_pc69'] = 10**(elem_ab_dic['NeH_pc69']-12.)/10**(elem_ab_dic['NeH_sum']-12.)
        icf_dic['NeO_pc69'] = 10**(elem_ab_dic['NeO_pc69'])/(ion_ab_dic['Ne3']/ion_ab_dic['O3'])
        icf_dic['NeH_s78'] = 10**(elem_ab_dic['NeH_s78']-12.)/10**(elem_ab_dic['NeH_sum']-12.)
        icf_dic['NeO_s78'] = 10**(elem_ab_dic['NeO_s78'])/(ion_ab_dic['Ne3']/ion_ab_dic['O3'])
        icf_dic['NeH_iz06'] = 10**(elem_ab_dic['NeH_iz06']-12.)/10**(elem_ab_dic['NeH_sum']-12.)
        icf_dic['NeO_iz06'] = 10**(elem_ab_dic['NeO_iz06'])/(ion_ab_dic['Ne3']/ion_ab_dic['O3'])
        icf_dic['NeH_ma19'] = 10**(elem_ab_dic['NeH_ma19']-12.)/10**(elem_ab_dic['NeH_sum']-12.)
        icf_dic['NeO_ma19'] = 10**(elem_ab_dic['NeO_ma19'])/(ion_ab_dic['Ne3']/ion_ab_dic['O2'])

        print "Abundancias de neon obtenida"

        ## Azufre ##
        # CASO PARA S+
        sum_s1 = ion_ab_dic['S2']
        icf1s1 = 10**((6.156)/(1.0+(0.450*np.exp(0.877*logO1O2)))-3.811) # ICF Medina-Amayo et al. (2019)
        icf2s1 = np.ones_like(sum_s1) # ICF Stasinska (1978)
        # Abundancias con ICFs
        S1H_1 = sum_s1
        S1O_2 = sum_s1*icf1s1/(ion_ab_dic['O3']) # Con ICF Medina-Amayo et al. (2019)
        S1H_2 = S1O_2*OH # Con ICF Medina-Amayo et al. (2019)
        S1O_5 = sum_s1*icf4s1/(ion_ab_dic['O2']) # Con ICF Stasinska (1978)
        S1H_5 = S1O_5*OH # Con ICF Stasinska (1978)
        # Abundancias
        elem_ab_dic['S1H_sum'] = 12. + np.log10(S1H_1)
        elem_ab_dic['S1O_ma19'] = np.log10(S1O_2)
        elem_ab_dic['S1H_ma19'] = 12. + np.log10(S1H_2)
        elem_ab_dic['S1O_s78'] = np.log10(S1O_5)
        elem_ab_dic['S1H_s78'] = 12. + np.log10(S1H_5)
        # Incertidumbres
        err_elem_ab_dic['S1O_ma19_inf'] = 0.33*np.ones_like(sum_o)
        err_elem_ab_dic['S1O_ma19_sup'] = 0.31*np.ones_like(sum_o)
        err_elem_ab_dic['S1O_s78_inf'] = 0.67*np.ones_like(sum_o)
        err_elem_ab_dic['S1O_s78_sup'] = 0.00*np.ones_like(sum_o)
        # Errores Medina-Amayo et al. (2019)
        esxS1O = 10**(elem_ab_dic['S1O_ma19'] + err_elem_ab_dic['S1O_ma19_sup']) - 10**elem_ab_dic['S1O_ma19']
        eixS1O = 10**elem_ab_dic['S1O_ma19'] - 10**(elem_ab_dic['S1O_ma19'] - err_elem_ab_dic['S1O_ma19_inf'])
        esxS1H = ((S1O_2+esxS1O)*(OH)) - (S1O_2*OH)
        eixS1H = (S1O_2*OH) - ((S1O_2-eixS1O)*(OH))
        err_elem_ab_dic['S1H_ma19_sup'] = np.log10(S1H_2+esxS1H) - np.log10(S1H_2)
        err_elem_ab_dic['S1H_ma19_inf'] = np.log10(S1H_2) - np.log10(S1H_2-eixS1H)
        # Errores Stasinska (1978)
        esxS1O_s = 10**(elem_ab_dic['S1O_s78'] + err_elem_ab_dic['S1O_s78_sup']) - 10**elem_ab_dic['S1O_s78']
        eixS1O_s = 10**elem_ab_dic['S1O_s78'] - 10**(elem_ab_dic['S1O_s78'] - err_elem_ab_dic['S1O_s78_inf'])
        esxS1H_s = ((S1O_5+esxS1O_s)*(OH)) - (S1O_5*OH)
        eixS1H_s = (S1O_5*OH) - ((S1O_5-eixS1O_s)*(OH))
        err_elem_ab_dic['S1H_s78_sup'] = np.log10(S1H_5+esxS1H_s) - np.log10(S1H_5)
        err_elem_ab_dic['S1H_s78_inf'] = np.log10(S1H_5) - np.log10(S1H_5-eixS1H_s)

        # CASO PARA S+ + S++
        sum_s12 = ion_ab_dic['S2'] + ion_ab_dic['S3']
        icf1s12 =  (1.-((1.-(ion_ab_dic['O2']/OH))**3.))**(-1./3) # Stasinska 1978 (caso para S+ + S++)
        for i, OH12 in zip(gio2, elem_ab_dic['OH']):
            if (OH12 < 7.2):
                icf2s12 =  (0.121*(i))+(0.511)+(0.161/(i)) # Izotov et al 2006 lowZ, lowZ= (logOH12 < 7.2)
            elif (OH12 > 7.2) & (OH12 < 8.2):
                icf2s12 =  (0.155*(i))+(0.849)+(0.062/(i))  # Izotov et al 2006 intZ, intZ= (logOH12 > 7.2) & (logOH12 < 8.2)
            elif (OH12 >= 8.2):
                icf2s12 =  (0.178*(i))+(0.610)+(0.153/(i))  # Izotov et al 2006 highZ, highZ= (logOH12 >= 8.2)
        icf3s12 = 10**(1.534*(nu**(0.299+(-0.296*nu)))-1.672) # Medina-Amayo et al. (2019)
        # Abundancias con ICFs
        S12H_1 = sum_s12
        S12H_2 = sum_s12*icf1s12 # Stasinska 1978
        S12O_2 = S12H_2/OH  # Stasinska 1978
        S12H_4 = sum_s12*icf2s12 # Izotov et al 2006
        S12O_4 = S12H_4/OH # Izotov et al 2006
        S12O_5 = sum_s12*icf3s12/ion_ab_dic['O2'] # Medina-Amayo et al. (2019)
        S12H_5 = S12O_5*OH # Medina-Amayo et al. (2019)
        # Abundancias
        elem_ab_dic['S12H_sum'] = 12. + np.log10(S12H_1)
        elem_ab_dic['S12H_s78'] = 12. + np.log10(S12H_2)
        elem_ab_dic['S12O_s78'] = np.log10(S12O_2)
        elem_ab_dic['S12H_iz06'] = 12. + np.log10(S12H_4)
        elem_ab_dic['S12O_iz06'] = np.log10(S12O_4)
        elem_ab_dic['S12O_ma19'] = np.log10(S12O_5)
        elem_ab_dic['S12H_ma19'] = 12. + np.log10(S12H_5)
        # Incertidumbres
        err_elem_ab_dic['S12H_s78_inf'] = 0.026*np.ones_like(sum_o) # Errores ICF s78
        err_elem_ab_dic['S12H_s78_sup'] = 0.096*np.ones_like(sum_o)
        if (OH12 < 7.2): # Errores ICF Izotov(2006)lowZ
            err_elem_ab_dic['S12H_iz06_inf'] = np.nan*np.ones_like(sum_o) # No pude calcular el error de este ICF con mis modelos
            err_elem_ab_dic['S12H_iz06_sup'] = np.nan*np.ones_like(sum_o) # No pude calcular el error de este ICF con mis modelos
        elif (OH12 > 7.2) & (OH12 < 8.2): # Errores ICF Izotov(2006)IntZ
            err_elem_ab_dic['S12H_iz06_inf'] = 0.01*np.ones_like(sum_o)
            err_elem_ab_dic['S12H_iz06_sup'] = 0.10*np.ones_like(sum_o)
        elif (OH12 >= 8.2): # Errores ICF Izotov(2006)HighZ
            err_elem_ab_dic['S12H_iz06_inf'] = 0.004*np.ones_like(sum_o)
            err_elem_ab_dic['S12H_iz06_sup'] = 0.050*np.ones_like(sum_o)
        err_elem_ab_dic['S12O_ma19_inf'] = 0.07*np.ones_like(sum_o)
        err_elem_ab_dic['S12O_ma19_sup'] = 0.06*np.ones_like(sum_o)
        # Errores Stasinska (1978)
        esxS12H_S = 10**(elem_ab_dic['S12H_s78'] + err_elem_ab_dic['S12H_s78_sup']) - 10**elem_ab_dic['S12H_s78']
        eixS12H_S = 10**elem_ab_dic['S12H_s78'] - 10**(elem_ab_dic['S12H_s78'] - err_elem_ab_dic['S12H_s78_inf'])
        esxS12O_S = ((S12H_2+esxS12H_S)/(OH)) - (S12H_2/OH)
        eixS12O_S = (S12H_2/OH) - ((S12H_2-eixS12H_S)/(OH))
        err_elem_ab_dic['S12O_s78_sup'] = np.log10(S12O_2+esxS12O_S) - np.log10(S12O_2)
        err_elem_ab_dic['S12O_s78_inf'] = np.log10(S12O_2) - np.log10(S12O_2-eixS12O_S)
        # Errores Izotov et al (2006)
        esxS12H_iz = 10**(elem_ab_dic['S12H_iz06'] + err_elem_ab_dic['S12H_iz06_sup']) - 10**elem_ab_dic['S12H_iz06']
        eixS12H_iz = 10**elem_ab_dic['S12H_iz06'] - 10**(elem_ab_dic['S12H_iz06'] - err_elem_ab_dic['S12H_iz06_inf'])
        esxS12O_iz = ((S12H_4+esxS12H_iz)/(OH)) - (S12H_4/OH)
        eixS12O_iz = (S12H_4/OH) - ((S12H_4-eixS12H_iz)/(OH))
        err_elem_ab_dic['S12O_iz06_sup'] = np.log10(S12O_4+esxS12O_iz) - np.log10(S12O_4)
        err_elem_ab_dic['S12O_iz06_inf'] = np.log10(S12O_4) - np.log10(S12O_4-eixS12O_iz)
        # Errores Medina-Amayo et al (2019)
        esxS12O = 10**(elem_ab_dic['S12O_ma19'] + err_elem_ab_dic['S12O_ma19_sup']) - 10**elem_ab_dic['S12O_ma19']
        eixS12O = 10**elem_ab_dic['S12O_ma19'] - 10**(elem_ab_dic['S12O_ma19'] - err_elem_ab_dic['S12O_ma19_inf'])
        esxS12H = ((S12O_6+esxS12O)*(OH)) - (S12O_6*OH)
        eixS12H = (S12O_6*OH) - ((S12O_6-eixS12O)*(OH))
        err_elem_ab_dic['S12H_ma19_sup'] = np.log10(S12H_6+esxS12H) - np.log10(S12H_6)
        err_elem_ab_dic['S12H_ma19_inf'] = np.log10(S12H_6) - np.log10(S12H_6-eixS12H)

        # ICFs azufre S+ + S++
        icf_dic['S12H_s78'] = 10**(elem_ab_dic['S12H_s78']-12.)/10**(elem_ab_dic['S12H_sum']-12.)
        icf_dic['S12O_s78'] = 10**(elem_ab_dic['S12O_s78'])/((ion_ab_dic['S2']+ion_ab_dic['S3'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        icf_dic['S12H_iz06'] = 10**(elem_ab_dic['S12H_iz06']-12.)/10**(elem_ab_dic['S12H_sum']-12.)
        icf_dic['S12O_iz06'] = 10**(elem_ab_dic['S12O_iz06'])/((ion_ab_dic['S2']+ion_ab_dic['S3'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        icf_dic['S12H_ma19'] = 10**(elem_ab_dic['S12H_ma19']-12.)/10**(elem_ab_dic['S12H_sum']-12.)
        icf_dic['S12O_ma19'] = 10**(elem_ab_dic['S12O_ma19'])/(ion_ab_dic['S2']+ion_ab_dic['S3']/ion_ab_dic['O2'])

        print "Abundancias de azufre obtenida"

        ## Cloro ##
        sum_cl = ion_ab_dic['Cl3']
        SHma19 = 10**(elem_ab_dic['S12H_ma19']-12.)
        s1o2 = (ion_ab_dic['S2']/SHma19)-(ion_ab_dic['O3']/OH)
        icf1cl2 =  1.0/(1.0-(s1o2)) # ICF Peimbert & Torres-Peimbert (1977)
        for i, OH12 in zip(gio2, elem_ab_dic['OH']):
            if (OH12 <= 7.2):
                icf2cl2 =  (0.756*(i))+(0.648)+(0.128/(i)) # Izotov et al 2006 lowZ, lowZ= (logOH12 < 7.2)
            elif (OH12 > 7.2) & (OH12 < 8.2):
                icf2cl2 =  (0.814*(i))+(0.620)+(0.131/(i))  # Izotov et al 2006 intZ, intZ= (logOH12 > 7.2) & (logOH12 < 8.2)
            elif (OH12 >= 8.2):
                icf2cl2 =  (1.186*(i))+(0.357)+(0.121/(i))  # Izotov et al 2006 highZ, highZ= (logOH12 >= 8.2)
        for xo2 in OH/ion_ab_dic['O3']: # Esteban et al. (2015) for 1.0 ≤ O/O++ ≤ 7.0
            if (xo2 >= 1.0) & (xo2 <= 7.0):
                icf3cl2 = 1.02 + 0.04*(xo2)
            else:
                ic3cl2 = np.nan
        icf4cl2 = 10**(((0.0011)*np.exp(6.51*gio2))+((-1.24)*np.exp(-3.6*gio2))) # Medina-Amayo et al.(2019)

        ClH_1 = sum_cl
        ClH_2 = sum_cl*icf1cl2 # Peimbert & Torres-Peimbert (1977)
        ClO_2 = ClH_2/OH # Peimbert & Torres-Peimbert (1977)
        ClH_3 = sum_cl*icf2cl2 # Izotov et al 2006
        ClO_3 = ClH_3/OH # Izotov et al 2006
        ClH_4 = sum_cl*icf3cl2 # Esteban et al. (2015)
        ClO_4 = ClH_4/OH # Esteban et al. (2015)
        ClO_5 = (sum_cl/ion_ab_dic['O2'])*icf4cl2 # Medina-Amayo et al. (2019)
        ClH_5 = ClO_5*OH # Medina-Amayo et al. (2019)

        elem_ab_dic['ClH_sum'] = 12. + np.log10(ClH_1)
        elem_ab_dic['ClH_ptp77'] = 12. + np.log10(ClH_2) # Peimbert & Torres-Peimbert (1977)
        elem_ab_dic['ClO_ptp77'] = np.log10(ClO_2) # Peimbert & Torres-Peimbert (1977)
        elem_ab_dic['ClH_iz06'] = 12. + np.log10(ClH_3) # Izotov et al 2006
        elem_ab_dic['ClO_iz06'] = np.log10(ClO_3) # Izotov et al 2006
        elem_ab_dic['ClH_es15'] = 12. + np.log10(ClH_4) # Esteban et al. (2015)
        elem_ab_dic['ClO_es15'] = np.log10(ClO_4) # Esteban et al. (2015)
        elem_ab_dic['ClH_ma19'] = 12. + np.log10(ClH_5) # Medina-Amayo et al. (2019)
        elem_ab_dic['ClO_ma19'] = np.log10(ClO_5) # Medina-Amayo et al. (2019)

        err_elem_ab_dic['ClH_ptp77_inf'] = 0.1*np.ones_like(sum_o)
        err_elem_ab_dic['ClH_ptp77_sup'] = 0.8*np.ones_like(sum_o)
        if (OH12 <= 7.2):
            err_elem_ab_dic['ClH_iz06_inf'] = np.nan*np.ones_like(sum_o)
            err_elem_ab_dic['ClH_iz06_sup'] = np.nan*np.ones_like(sum_o)
        elif (OH12 > 7.2) & (OH12 < 8.2):
            err_elem_ab_dic['ClH_iz06_inf'] = 0.4*np.ones_like(sum_o)
            err_elem_ab_dic['ClH_iz06_sup'] = 0.4*np.ones_like(sum_o)
        elif (OH12 >= 8.2):
            err_elem_ab_dic['ClH_iz06_inf'] = 0.6*np.ones_like(sum_o)
            err_elem_ab_dic['ClH_iz06_sup'] = 0.4*np.ones_like(sum_o)
        err_elem_ab_dic['ClH_es15_inf'] = 0.11*np.ones_like(sum_o)
        err_elem_ab_dic['ClH_es15_sup'] = 0.05*np.ones_like(sum_o)
        err_elem_ab_dic['ClO_ma19_inf'] = 0.4*np.ones_like(sum_o)
        err_elem_ab_dic['ClO_ma19_sup'] = 0.2*np.ones_like(sum_o)
        # Errores Peimbert & Torres-Peimbert (1977)
        esxClH_ptp = 10**(elem_ab_dic['ClH_ptp77'] + err_elem_ab_dic['ClH_ptp77_sup']) - 10**elem_ab_dic['ClH_ptp77']
        eixClH_ptp = 10**elem_ab_dic['ClH_ptp77'] - 10**(elem_ab_dic['ClH_ptp77'] - err_elem_ab_dic['ClH_ptp77_inf'])
        esxCl2O_pt = ((Cl2H_2+esxClH_ptp)/(OH)) - (Cl2H_2/OH)
        eixCl2O_pt = (Cl2H_2/OH) - ((Cl2H_2-eixClH_ptp)/(OH))
        err_elem_ab_dic['Cl2O_ptp_sup'] = np.log10(Cl2O_2+esxCl2O_pt) - np.log10(Cl2O_2)
        err_elem_ab_dic['Cl2O_ptp_inf'] = np.log10(Cl2O_2) - np.log10(Cl2O_2-eixCl2O_pt)
        # Errores Izotov et al 2006
        esxClH_iz06 = 10**(elem_ab_dic['ClH_iz06'] + err_elem_ab_dic['ClH_iz06_sup']) - 10**elem_ab_dic['ClH_iz06']
        eixClH_iz06 = 10**elem_ab_dic['ClH_iz06'] - 10**(elem_ab_dic['ClH_iz06'] - err_elem_ab_dic['ClH_iz06_inf'])
        esxCl2O_iz06 = ((Cl2H_2+esxClH_iz06)/(OH)) - (Cl2H_2/OH)
        eixCl2O_iz06 = (Cl2H_2/OH) - ((Cl2H_2-eixClH_iz06)/(OH))
        err_elem_ab_dic['Cl2O_iz06_sup'] = np.log10(Cl2O_2+esxCl2O_iz06) - np.log10(Cl2O_2)
        err_elem_ab_dic['Cl2O_iz06_inf'] = np.log10(Cl2O_2) - np.log10(Cl2O_2-eixCl2O_iz06)
        # Errores Esteban et al. (2015)
        esxClH_es15 = 10**(elem_ab_dic['ClH_es15'] + err_elem_ab_dic['ClH_es15_sup']) - 10**elem_ab_dic['ClH_es15']
        eixClH_es15 = 10**elem_ab_dic['ClH_es15'] - 10**(elem_ab_dic['ClH_es15'] - err_elem_ab_dic['ClH_es15_inf'])
        esxCl2O_es15 = ((Cl2H_2+esxClH_es15)/(OH)) - (Cl2H_2/OH)
        eixCl2O_es15 = (Cl2H_2/OH) - ((Cl2H_2-eixClH_es15)/(OH))
        err_elem_ab_dic['Cl2O_es15_sup'] = np.log10(Cl2O_2+esxCl2O_es15) - np.log10(Cl2O_2)
        err_elem_ab_dic['Cl2O_es15_inf'] = np.log10(Cl2O_2) - np.log10(Cl2O_2-eixCl2O_es15)
        # Errores Medina-Amayo et al. (2019)
        esxCl2O = 10**(elem_ab_dic['Cl2O_ma19'] + err_elem_ab_dic['Cl2O_ma19_sup']) - 10**elem_ab_dic['Cl2O_ma19']
        eixCl2O = 10**elem_ab_dic['Cl2O_ma19'] - 10**(elem_ab_dic['Cl2O_ma19'] - err_elem_ab_dic['Cl2O_ma19_inf'])
        esxCl2H = ((Cl2O_6+esxCl2O)*(OH)) - (Cl2O_6*OH)
        eixCl2H = (Cl2O_6*OH) - ((Cl2O_6-eixCl2O)*(OH))
        err_elem_ab_dic['Cl2H_ma19_sup'] = np.log10(Cl2H_6+esxCl2H) - np.log10(Cl2H_6)
        err_elem_ab_dic['Cl2H_ma19_inf'] = np.log10(Cl2H_6) - np.log10(Cl2H_6-eixCl2H)

        # ICFs Cl #
        icf_dic['ClH_ptp77'] = 10**(elem_ab_dic['NH_pc69']-12.)/10**(elem_ab_dic['ClH_sum']-12.) # icf[NO_KB94] = 1.0
        icf_dic['ClH_ma19'] = 10**(elem_ab_dic['NH_ma19']-12.)/10**(elem_ab_dic['ClH_sum']-12.)
        icf_dic['ClO_ma19'] = 10**(elem_ab_dic['NO_ma19'])/(ion_ab_dic['N2']/ion_ab_dic['O2'])

        print "Abundancias de cloro obtenida"


        ## Argon ##
        # CASO PARA Ar++
        sum_ar2 = ion_ab_dic['Ar3']
        O1_O= (ion_ab_dic['O2']/OH)
        sgio = ion_ab_dic['S3']/(ion_ab_dic['S2']+ion_ab_dic['S3'])
        icf1ar2 = (0.15+(O1_O*(2.39-2.64*O1_O)))**(-1.0) # Izotov et al 1994
        icf2ar2 = 0.596+(0.967*(1-gio))+(0.077/(1-gio)) # Perez-Montero et al 2007
        icf3ar2 = 0.596+(0.967*(1-sgio))+(0.077/(1-sgio)) # Perez-Montero et al 2007  [S++/(S+ + S++)]
        for i, OH12 in zip(gio2, elem_ab_dic['OH']):
            if (OH12 < 7.2):
                icf4ar2 =  (0.278*(i))+(0.836)+(0.051/(i)) # Izotov et al 2006 lowZ, lowZ= (logOH12 < 7.2)
            elif (OH12 > 7.2) & (OH12 < 8.2):
                icf4ar2 =  (0.285*(i))+(0.833)+(0.051/(i))  # Izotov et al 2006 intZ, intZ= (logOH12 > 7.2) & (logOH12 < 8.2)
            elif (OH12 >= 8.2):
                icf4ar2 =  (0.517*(i))+(0.763)+(0.042/(i))  # Izotov et al 2006 highZ, highZ= (logOH12 >= 8.2)
        icf5ar2 = 10**(((0.502)*(gio**(14.3/gio)))+(0.39*(np.log(gio)))+0.03) # Medina-Amayo et al (2019)
        # Abundancias con ICFs
        Ar2H_1 = sum_ar2
        Ar2H_2 = sum_ar2*icf1ar2 # Izotov et al 1994
        Ar2O_2 = Ar2H_2/OH # Izotov et al 1994
        Ar2H_3 = sum_ar2*icf2ar2 # Perez-Montero et al 2007  [O++/(O+ + O++)]
        Ar2O_3 = Ar2H_3/OH # Perez-Montero et al 2007  [O++/(O+ + O++)]
        Ar2H_4 = sum_ar2*icf3ar2 # Perez-Montero et al 2007  [S++/(S+ + S++)]
        Ar2O_4 = Ar2H_4/OH # Perez-Montero et al 2007  [S++/(S+ + S++)]
        Ar2H_5 = sum_ar2*icf4ar2 # Izotov et al 2006
        Ar2O_5 = Ar2H_5/OH # Izotov et al 2006
        Ar2O_7 = sum_ar2*icf5ar2/ion_ab_dic['O3'] # Medina-Amayo et al (2019)
        Ar2H_7 = Ar2O_7*OH # Medina-Amayo et al (2019)
        # Abundancias
        elem_ab_dic['Ar2H_sum'] = 12. + np.log10(Ar2H_1)
        elem_ab_dic['Ar2H_iz94'] = 12. + np.log10(Ar2H_2)
        elem_ab_dic['Ar2O_iz94'] = np.log10(Ar2O_2)
        elem_ab_dic['Ar2H_PMgio'] = 12. + np.log10(Ar2H_3)
        elem_ab_dic['Ar2O_PMgio'] = np.log10(Ar2O_3)
        elem_ab_dic['Ar2H_PMsgio'] = 12. + np.log10(Ar2H_4)
        elem_ab_dic['Ar2O_PMsgio'] = np.log10(Ar2O_4)
        elem_ab_dic['Ar2H_iz06'] = 12. + np.log10(Ar2H_5)
        elem_ab_dic['Ar2O_iz06'] = np.log10(Ar2O_5)
        elem_ab_dic['Ar2O_ma19'] = np.log10(Ar2O_7)
        elem_ab_dic['Ar2H_ma19'] = 12. + np.log10(Ar2H_7)
        # Incertidumbres
        err_elem_ab_dic['Ar2H_iz94_inf'] = 0.10*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2H_iz94_sup'] = 0.42*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2H_PMgio_inf'] = 0.16*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2H_PMgio_sup'] = 0.20*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2H_PMsgio_inf'] = 0.32*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2H_PMsgio_sup'] = 0.41*np.ones_like(sum_o)
        if (OH12 < 7.2): # Errores ICF Izotov(2006)lowZ
            err_elem_ab_dic['Ar2H_iz06_inf'] = np.nan # No pude calcular el error de este ICF con mis modelos
            err_elem_ab_dic['Ar2H_iz06_sup'] = np.nan # No pude calcular el error de este ICF con mis modelos
        elif (OH12 > 7.2) & (OH12 < 8.2): # Errores ICF Izotov(2006)IntZ
            err_elem_ab_dic['Ar2H_iz06_inf'] = 0.14*np.ones_like(sum_o)
            err_elem_ab_dic['Ar2H_iz06_sup'] = 0.05*np.ones_like(sum_o)
        elif (OH12 >= 8.2): # Errores ICF Izotov(2006)HighZ
            err_elem_ab_dic['Ar2H_iz06_inf'] = 0.12*np.ones_like(sum_o)
            err_elem_ab_dic['Ar2H_iz06_sup'] = 0.01*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2O_ma19_inf'] = 0.14*np.ones_like(sum_o)
        err_elem_ab_dic['Ar2O_ma19_sup'] = 0.10*np.ones_like(sum_o)
        # Errores Izotov et al. (1994)
        esxAr2H_I9 = 10**(elem_ab_dic['Ar2H_iz94'] + err_elem_ab_dic['Ar2H_iz94_sup']) - 10**elem_ab_dic['Ar2H_iz94']
        eixAr2H_I9 = 10**elem_ab_dic['Ar2H_iz94'] - 10**(elem_ab_dic['Ar2H_iz94'] - err_elem_ab_dic['Ar2H_iz94_inf'])
        esxAr2O_I9 = ((Ar2H_2+esxAr2H_I9)/(OH)) - (Ar2H_2/OH)
        eixAr2O_I9 = (Ar2H_2/OH) - ((Ar2H_2-eixAr2H_I9)/(OH))
        err_elem_ab_dic['Ar2O_iz94_sup'] = np.log10(Ar2O_2+esxAr2O_I9) - np.log10(Ar2O_2)
        err_elem_ab_dic['Ar2O_iz94_inf'] = np.log10(Ar2O_2) - np.log10(Ar2O_2-eixAr2O_I9)
        # Errores Perez-Montero et al. (2007)
        esxAr2H_PM = 10**(elem_ab_dic['Ar2H_PMgio'] + err_elem_ab_dic['Ar2H_PMgio_sup']) - 10**elem_ab_dic['Ar2H_PMgio']
        eixAr2H_PM = 10**elem_ab_dic['Ar2H_PMgio'] - 10**(elem_ab_dic['Ar2H_PMgio'] - err_elem_ab_dic['Ar2H_PMgio_inf'])
        esxAr2O_PM = ((Ar2H_3+esxAr2H_PM)/(OH)) - (Ar2H_3/OH)
        eixAr2O_PM = (Ar2H_3/OH) - ((Ar2H_3-eixAr2H_PM)/(OH))
        err_elem_ab_dic['Ar2O_PMgio_sup'] = np.log10(Ar2O_3+esxAr2O_PM) - np.log10(Ar2O_3)
        err_elem_ab_dic['Ar2O_PMgio_inf'] = np.log10(Ar2O_3) - np.log10(Ar2O_3-eixAr2O_PM)
        # Errores Perez-Montero et al. (2007)
        esxAr2H_PMs = 10**(elem_ab_dic['Ar2H_PMsgio'] + err_elem_ab_dic['Ar2H_PMsgio_sup']) - 10**elem_ab_dic['Ar2H_PMsgio']
        eixAr2H_PMs = 10**elem_ab_dic['Ar2H_PMsgio'] - 10**(elem_ab_dic['Ar2H_PMsgio'] - err_elem_ab_dic['Ar2H_PMsgio_inf'])
        esxAr2O_PMs = ((Ar2H_4+esxAr2H_PMs)/(OH)) - (Ar2H_4/OH)
        eixAr2O_PMs = (Ar2H_4/OH) - ((Ar2H_4-eixAr2H_PMs)/(OH))
        err_elem_ab_dic['Ar2O_PMsgio_sup'] = np.log10(Ar2O_4+esxAr2O_PMs) - np.log10(Ar2O_4)
        err_elem_ab_dic['Ar2O_PMsgio_inf'] = np.log10(Ar2O_4) - np.log10(Ar2O_4-eixAr2O_PMs)
        # Errores Izotov et al. (2006)
        esxAr2H_iz = 10**(elem_ab_dic['Ar2H_iz06'] + err_elem_ab_dic['Ar2H_iz06_sup']) - 10**elem_ab_dic['Ar2H_iz06']
        eixAr2H_iz = 10**elem_ab_dic['Ar2H_iz06'] - 10**(elem_ab_dic['Ar2H_iz06'] - err_elem_ab_dic['Ar2H_iz06_inf'])
        esxAr2O_iz = ((Ar2H_5+esxAr2H_iz)/(OH)) - (Ar2H_5/OH)
        eixAr2O_iz = (Ar2H_5/OH) - ((Ar2H_5-eixAr2H_iz)/(OH))
        err_elem_ab_dic['Ar2O_iz06_sup'] = np.log10(Ar2O_5+esxAr2O_iz) - np.log10(Ar2O_5)
        err_elem_ab_dic['Ar2O_iz06_inf'] = np.log10(Ar2O_5) - np.log10(Ar2O_5-eixAr2O_iz)
        # Errores Medina-Amayo et al. (2019)
        esxAr2O = 10**(elem_ab_dic['Ar2O_ma19'] + err_elem_ab_dic['Ar2O_ma19_sup']) - 10**elem_ab_dic['Ar2O_ma19']
        eixAr2O = 10**elem_ab_dic['Ar2O_ma19'] - 10**(elem_ab_dic['Ar2O_ma19'] - err_elem_ab_dic['Ar2O_ma19_inf'])
        esxAr2H = ((Ar2O_7+esxAr2O)*(OH)) - (Ar2O_7*OH)
        eixAr2H = (Ar2O_7*OH) - ((Ar2O_7-eixAr2O)*(OH))
        err_elem_ab_dic['Ar2H_ma19_sup'] = np.log10(Ar2H_7+esxAr2H) - np.log10(Ar2H_7)
        err_elem_ab_dic['Ar2H_ma19_inf'] = np.log10(Ar2H_7) - np.log10(Ar2H_7-eixAr2H)

        # CASO PARA Ar++ + Ar+3
        sum_ar23 = ion_ab_dic['Ar3'] + ion_ab_dic['Ar4']
        icf1ar23 = (0.99+(O1_O*(0.091+(O1_O*(-1.14+0.077*O1_O)))))**(-1.0) # Izotov et al 1994
        icf2ar23 = 0.928+(0.364*(1.0-gio))+(0.006/(1.0-gio)) # Perez-Montero et al 2007  [O++/(O+ + O++)]
        icf3ar23 = 0.870+(0.695*(1.0-sgio))+(0.0086/(1.0-sgio)) # Perez-Montero et al 2007  [S++/(S+ + S++)]
        for i, OH12 in zip(gio2, elem_ab_dic['OH']):
            if (OH12 < 7.2):
                icf4ar23 =  (0.158*(i))+(0.958)+(0.004/(i)) # Izotov et al 2006 lowZ, lowZ= (logOH12 < 7.2)
            elif (OH12 > 7.2) & (OH12 < 8.2):
                icf4ar23 =  (0.104*(i))+(0.980)+(0.001/(i))  # Izotov et al 2006 intZ, intZ= (logOH12 > 7.2) & (logOH12 < 8.2)
            elif (OH12 >= 8.2):
                icf4ar23 =  (0.238*(i))+(0.931)+(0.004/(i))  # Izotov et al 2006 highZ, highZ= (logOH12 >= 8.2)
        icf6ar23 = 10**((-5.38)*((gio+0.0061)**(-0.066))+5.37) # Medina-Amayo et al. (2019)

        Ar23H_1 = sum_ar23
        Ar23H_2 = sum_ar23*icf1ar23 # Izotov et al 1994
        Ar23O_2 = Ar23H_2/OH # Izotov et al 1994
        Ar23H_3 = sum_ar23*icf2ar23 # Perez-Montero et al 2007  [O++/(O+ + O++)]
        Ar23O_3 = Ar23H_3/OH # Perez-Montero et al 2007  [O++/(O+ + O++)]
        Ar23H_4 = sum_ar23*icf3ar23 # Perez-Montero et al 2007  [S++/(S+ + S++)]
        Ar23O_4 = Ar23H_4/OH # Perez-Montero et al 2007  [S++/(S+ + S++)]
        Ar23H_5 = sum_ar23*icf4ar23 # Izotov et al 2006
        Ar23O_5 = Ar23H_5/OH # Izotov et al 2006
        Ar23O_7 = sum_ar23*icf6ar23/ion_ab_dic['O3'] # Medina-Amayo et al. (2019)
        Ar23H_7 = Ar23O_7*OH # Medina-Amayo et al. (2019)

        elem_ab_dic['Ar23H_sum'] = 12. + np.log10(Ar23H_1)
        elem_ab_dic['Ar23H_iz94'] = 12. + np.log10(Ar23H_2)
        elem_ab_dic['Ar23O_iz94'] = np.log10(Ar23O_2)
        elem_ab_dic['Ar23H_PMgio'] = 12. + np.log10(Ar23H_3)
        elem_ab_dic['Ar23O_PMgio'] = np.log10(Ar23O_3)
        elem_ab_dic['Ar23H_PMsgio'] = 12. + np.log10(Ar23H_4)
        elem_ab_dic['Ar23O_PMsgio'] = np.log10(Ar23O_4)
        elem_ab_dic['Ar23H_iz06'] = 12. + np.log10(Ar23H_5)
        elem_ab_dic['Ar23O_iz06'] = np.log10(Ar23O_5)
        elem_ab_dic['Ar23O_ma19'] = np.log10(Ar23O_7)
        elem_ab_dic['Ar23H_ma19'] = 12. + np.log10(Ar23H_7)

        err_elem_ab_dic['Ar23H_iz94_inf'] = 0.08*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23H_iz94_sup'] = 0.36*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23H_PMgio_inf'] = 0.067*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23H_PMgio_sup'] = 0.25*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23H_PMsgio_inf'] = 0.09*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23H_PMsgio_sup'] = 0.25*np.ones_like(sum_o)

        if (OH12 < 7.2): # Errores ICF Izotov(2006)lowZ
            err_elem_ab_dic['Ar2H_iz06_inf'] = np.nan # No pude calcular el error de este ICF con mis modelos
            err_elem_ab_dic['Ar2H_iz06_sup'] = np.nan # No pude calcular el error de este ICF con mis modelos
        elif (OH12 > 7.2) & (OH12 < 8.2): # Errores ICF Izotov(2006)IntZ
            err_elem_ab_dic['Ar23H_iz06_inf'] = 0.080*np.ones_like(sum_o)
            err_elem_ab_dic['Ar23H_iz06_sup'] = 0.002*np.ones_like(sum_o)
        elif (OH12 >= 8.2): # Errores ICF Izotov(2006)HighZ
            err_elem_ab_dic['Ar23H_iz06_inf'] = 0.13*np.ones_like(sum_o)
            err_elem_ab_dic['Ar23H_iz06_sup'] = 0.02*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23O_ma19_inf'] = 0.04*np.ones_like(sum_o)
        err_elem_ab_dic['Ar23O_ma19_sup'] = 0.03*np.ones_like(sum_o)

        # Errores Izotov et al (1994)
        esxAr23H_I9 = 10**(elem_ab_dic['Ar23H_iz94'] + err_elem_ab_dic['Ar23H_iz94_sup']) - 10**elem_ab_dic['Ar23H_iz94']
        eixAr23H_I9 = 10**elem_ab_dic['Ar23H_iz94'] - 10**(elem_ab_dic['Ar23H_iz94'] - err_elem_ab_dic['Ar23H_iz94_inf'])
        esxAr23O_I9 = ((Ar23H_2+esxAr23H_I9)/(OH)) - (Ar23H_2/OH)
        eixAr23O_I9 = (Ar23H_2/OH) - ((Ar23H_2-eixAr23H_I9)/(OH))
        err_elem_ab_dic['Ar23O_iz94_sup'] = np.log10(Ar23O_2+esxAr23O_I9) - np.log10(Ar23O_2)
        err_elem_ab_dic['Ar23O_iz94_inf'] = np.log10(Ar23O_2) - np.log10(Ar23O_2-eixAr23O_I9)
        # Errores Perez-Montero et al. (2007)
        esxAr23H_PM = 10**(elem_ab_dic['Ar23H_PMgio'] + err_elem_ab_dic['Ar23H_PMgio_sup']) - 10**elem_ab_dic['Ar23H_PMgio']
        eixAr23H_PM = 10**elem_ab_dic['Ar23H_PMgio'] - 10**(elem_ab_dic['Ar23H_PMgio'] - err_elem_ab_dic['Ar23H_PMgio_inf'])
        esxAr23O_PM = ((Ar23H_3+esxAr23H_PM)/(OH)) - (Ar23H_3/OH)
        eixAr23O_PM = (Ar23H_3/OH) - ((Ar23H_3-eixAr23H_PM)/(OH))
        err_elem_ab_dic['Ar23O_PMgio_sup'] = np.log10(Ar23O_3+esxAr23O_PM) - np.log10(Ar23O_3)
        err_elem_ab_dic['Ar23O_PMgio_inf'] = np.log10(Ar23O_3) - np.log10(Ar23O_3-eixAr23O_PM)
        # Errores Perez-Montero et al. (2007)
        esxAr23H_PMs = 10**(elem_ab_dic['Ar23H_PMsgio'] + err_elem_ab_dic['Ar23H_PMsgio_sup']) - 10**elem_ab_dic['Ar23H_PMsgio']
        eixAr23H_PMs = 10**elem_ab_dic['Ar23H_PMsgio'] - 10**(elem_ab_dic['Ar23H_PMsgio'] - err_elem_ab_dic['Ar23H_PMsgio_inf'])
        esxAr23O_PMs = ((Ar23H_4+esxAr23H_PMs)/(OH)) - (Ar23H_4/OH)
        eixAr23O_PMs = (Ar23H_4/OH) - ((Ar23H_4-eixAr23H_PMs)/(OH))
        err_elem_ab_dic['Ar23O_PMsgio_sup'] = np.log10(Ar23O_4+esxAr23O_PMs) - np.log10(Ar23O_4)
        err_elem_ab_dic['Ar23O_PMsgio_inf'] = np.log10(Ar23O_4) - np.log10(Ar23O_4-eixAr23O_PMs)
        # Errores Izotov et al (2006)
        esxAr23H_IZI = 10**(elem_ab_dic['Ar23H_iz06'] + err_elem_ab_dic['Ar23H_iz06_sup']) - 10**elem_ab_dic['Ar23H_iz06']
        eixAr23H_IZI = 10**elem_ab_dic['Ar23H_iz06'] - 10**(elem_ab_dic['Ar23H_iz06'] - err_elem_ab_dic['Ar23H_iz06_inf'])
        esxAr23O_IZI = ((Ar23H_5+esxAr23H_IZI)/(OH)) - (Ar23H_5/OH)
        eixAr23O_IZI = (Ar23H_5/OH) - ((Ar23H_5-eixAr23H_IZI)/(OH))
        err_elem_ab_dic['Ar23O_iz06_sup'] = np.log10(Ar23O_5+esxAr23O_IZI) - np.log10(Ar23O_5)
        err_elem_ab_dic['Ar23O_iz06_inf'] = np.log10(Ar23O_5) - np.log10(Ar23O_5-eixAr23O_IZI)
        # Errores Medina-Amayo et al. (2019)
        esxAr23Od = 10**(elem_ab_dic['Ar23O_ma19'] + err_elem_ab_dic['Ar23O_ma19_sup']) - 10**elem_ab_dic['Ar23O_ma19']
        eixAr23Od = 10**elem_ab_dic['Ar23O_ma19'] - 10**(elem_ab_dic['Ar23O_ma19'] - err_elem_ab_dic['Ar23O_ma19_inf'])
        esxAr23Hd = ((Ar23O_7+esxAr23Od)*(OH)) - (Ar23O_7*OH)
        eixAr23Hd = (Ar23O_7*OH) - ((Ar23O_7-eixAr23Od)*(OH))
        err_elem_ab_dic['Ar23H_ma19_sup'] = np.log10(Ar23H_7+esxAr23Hd) - np.log10(Ar23H_7)
        err_elem_ab_dic['Ar23H_ma19_inf'] = np.log10(Ar23H_7) - np.log10(Ar23H_7-eixAr23Hd)

        # ICFs
        icf_dic['Ar23H_iz94'] = 10**(elem_ab_dic['Ar23H_iz94']-12.)/10**(elem_ab_dic['Ar23H_sum']-12.)
        icf_dic['Ar23O_iz94'] = 10**(elem_ab_dic['Ar23O_iz94'])/((ion_ab_dic['Ar3']+ion_ab_dic['Ar4'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        icf_dic['Ar23H_PMgio'] = 10**(elem_ab_dic['Ar23H_PMgio']-12.)/10**(elem_ab_dic['Ar23H_sum']-12.)
        icf_dic['Ar23O_PMgio'] = 10**(elem_ab_dic['Ar23O_PMgio'])/((ion_ab_dic['Ar3']+ion_ab_dic['Ar4'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        icf_dic['Ar23H_PMsgio'] = 10**(elem_ab_dic['Ar23H_PMsgio']-12.)/10**(elem_ab_dic['Ar23H_sum']-12.)
        icf_dic['Ar23O_PMsgio'] = 10**(elem_ab_dic['Ar23O_PMsgio'])/((ion_ab_dic['Ar3']+ion_ab_dic['Ar4'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        icf_dic['Ar23H_iz06'] = 10**(elem_ab_dic['Ar23H_iz06']-12.)/10**(elem_ab_dic['Ar23H_sum']-12.)
        icf_dic['Ar23O_iz06'] = 10**(elem_ab_dic['Ar23O_iz06'])/((ion_ab_dic['Ar3']+ion_ab_dic['Ar4'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        icf_dic['Ar23H_ma19'] = 10**(elem_ab_dic['Ar23H_ma19']-12.)/10**(elem_ab_dic['Ar23H_sum']-12.)
        icf_dic['Ar23O_ma19'] = 10**(elem_ab_dic['Ar23O_ma19'])/((ion_ab_dic['Ar3']+ion_ab_dic['Ar4'])/(ion_ab_dic['O2']+ion_ab_dic['O3']))
        print "Abundancias de argon obtenida"


        print "Abundancias totales obtenidas"
        return ion_ab_dic, elem_ab_dic, icf_dic


#=========================================MAIN====================================================

def main_(out_file = './final_calculation/Prueba1.dat', NMC = 500):
    obj = abund_quim(out_file, NMonteCarlo = NMC)
    temp_O3, temp_O2, temp, den, dens = obj.getTemDen()
    ion_ab_dic, elem_ab_dic = obj.getElemAb()

    param = {'temp_O3': temp_O3, 'temp_O2':temp_O2, 'den':den}
    param.update(dens)

    params = get_reduced_dic(NMC, obj.obs.n_obs_origin, param, value_method='median')
    ion_abundances = get_reduced_dic(NMC, obj.obs.n_obs_origin, ion_ab_dic, value_method='original', error_method='std', abund12=True)
    elem_abundances = get_reduced_dic(NMC, obj.obs.n_obs_origin, elem_ab_dic, value_method='original', abund12=True)


    return params, ion_abundances, elem_abundances, obj

def reduce_params(output_file, input_file = 'all_windo.dat', NMC=2):
    params, ion_abundances, elem_abundances, obj = main_(out_file = input_file, NMC = NMC)
    chem_abund = pd.DataFrame()
    chem_abund['ID'] = obj.obs.names[:obj.obs.n_obs_origin]
    chem_abund['temp_O3'] = params['temp_O3']
    chem_abund['temp_O3_e'] = params['temp_O3_e']

    chem_abund['temp_O2'] = params['temp_O2']
    chem_abund['temp_O2_e'] = params['temp_O2_e']

    chem_abund['densidad'] = params['den']
    chem_abund['densidad_e'] = params['den_e']

    for i in range(len(ion_abundances)):
        chem_abund['ionabund_%s'%ion_abundances.keys()[i]] = ion_abundances.values()[i]
    for i in range(len(elem_abundances)):
        chem_abund['abund_%s'%elem_abundances.keys()[i]] = elem_abundances.values()[i]

    chem_abund.to_csv(output_file, mode = 'w', index = False)
    return chem_abund
